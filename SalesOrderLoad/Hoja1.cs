﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using SalesOrderLoad.SalesOrderWS;
using System.Collections.Generic;
using System.Linq;

namespace SalesOrderLoad
{
    public partial class Hoja1
    {
        private void Hoja1_Startup(object sender, System.EventArgs e)
        {
        }

        private void Hoja1_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.Startup += new System.EventHandler(this.Hoja1_Startup);
            this.Shutdown += new System.EventHandler(this.Hoja1_Shutdown);

        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {

            Excel.Worksheet headerSheet = ((Excel.Worksheet)this.Application.ActiveWorkbook.Sheets[1]);
            Excel.Range rng = headerSheet.Range["A6:O100000"];


            Excel.Worksheet itemSheet = ((Excel.Worksheet)this.Application.ActiveWorkbook.Sheets[2]);


            Excel.Range item = itemSheet.Range["A2:M100000"];

            Excel.Range outMessageMain = headerSheet.get_Range("Q6");


            List<Excel.Range> listaHeaders = new List<Microsoft.Office.Interop.Excel.Range>();
            List<Excel.Range> listaItems = new List<Microsoft.Office.Interop.Excel.Range>();

            foreach (Excel.Range it in item.Rows)
            {

                listaItems.Add(it);

            }

            foreach (Excel.Range hdr in rng.Rows)
            {

                listaHeaders.Add(hdr);

            }

            var filterheaders = listaHeaders.Where(n => n.Range["A1"].Value2 != null);

            outMessageMain.Value2 += "\nProcessing headers";

            var totalHeaders = filterheaders.Count();

            var counter = 1;
            foreach (Excel.Range c in filterheaders)
            {
                //Web service instance


                outMessageMain.Value2 = "Processing: " + counter.ToString() + "/" + totalHeaders.ToString();


                SalesOrderMaintainRequestBundleMessage_sync request = new SalesOrderMaintainRequestBundleMessage_sync();



                request.SalesOrder = new SalesOrderMaintainRequest[1];
                request.SalesOrder[0] = new SalesOrderMaintainRequest();
                request.SalesOrder[0].actionCode = ActionCode.Item01;


                SalesOrderMaintainRequestBundleMessage_sync requestUpdate = new SalesOrderMaintainRequestBundleMessage_sync();

                requestUpdate.SalesOrder = new SalesOrderMaintainRequest[1];
                requestUpdate.SalesOrder[0] = new SalesOrderMaintainRequest();
                requestUpdate.SalesOrder[0].actionCode = ActionCode.Item02;


                var orderID = "";
                var account = "";
                var shipTo = "";
                var salesUnit = "";
                var employee = "";
                var distributionChannel = "";
                var externalSalesPartner = "";
                var description = "";
                var externalReference = "";
                var postingDate = "";
                var requestedDate = "";
                var currency = "";
                var deliveryPriority = "";
                var customerNotes = "";
                var internalComments = "";

                if (c.Range["A1"].Value2 != null)
                {

                    orderID = c.Range["A1"].Value2.ToString();

                    /*
                    request.SalesOrder[0].ID = new BusinessTransactionDocumentID();
                    request.SalesOrder[0].ID.Value = orderID;
                    */

                }

                if (c.Range["B1"].Value2 != null)
                {

                    account = c.Range["B1"].Value2.ToString();
                    request.SalesOrder[0].BuyerID = new BusinessTransactionDocumentID();
                    request.SalesOrder[0].BuyerID.Value = account;

                    request.SalesOrder[0].AccountParty = new SalesOrderMaintainRequestPartyParty();
                    request.SalesOrder[0].AccountParty.PartyID = new PartyID();
                    request.SalesOrder[0].AccountParty.PartyID.Value = account;


                }

                if (c.Range["C1"].Value2 != null)
                {

                    shipTo = c.Range["C1"].Value2.ToString();
                    request.SalesOrder[0].ProductRecipientParty = new SalesOrderMaintainRequestPartyParty();
                    request.SalesOrder[0].ProductRecipientParty.PartyID = new PartyID();
                    request.SalesOrder[0].ProductRecipientParty.PartyID.Value = shipTo;



                }

                if (c.Range["D1"].Value2 != null)
                {

                    salesUnit = c.Range["D1"].Value2.ToString();
                    request.SalesOrder[0].SalesUnitParty = new SalesOrderMaintainRequestPartyIDParty();
                    request.SalesOrder[0].SalesUnitParty.actionCode = ActionCode.Item01;
                    request.SalesOrder[0].SalesUnitParty.PartyID = new PartyID();
                    request.SalesOrder[0].SalesUnitParty.PartyID.Value = salesUnit;


                }

                if (c.Range["E1"].Value2 != null)
                {

                    employee = c.Range["E1"].Value2.ToString();
                    request.SalesOrder[0].EmployeeResponsibleParty = new SalesOrderMaintainRequestPartyIDParty();
                    request.SalesOrder[0].EmployeeResponsibleParty.PartyID = new PartyID();
                    request.SalesOrder[0].EmployeeResponsibleParty.PartyID.Value = employee;


                }

                if (c.Range["F1"].Value2 != null)
                {

                    distributionChannel = c.Range["F1"].Value2.ToString();
                    request.SalesOrder[0].SalesAndServiceBusinessArea = new SalesOrderMaintainRequestSalesAndServiceBusinessArea();
                    request.SalesOrder[0].SalesAndServiceBusinessArea.actionCode = ActionCode.Item01;
                    request.SalesOrder[0].SalesAndServiceBusinessArea.DistributionChannelCode = new DistributionChannelCode();
                    request.SalesOrder[0].SalesAndServiceBusinessArea.DistributionChannelCode.Value = distributionChannel;

                }

                if (c.Range["G1"].Value2 != null)
                {

                    externalSalesPartner = c.Range["G1"].Value2.ToString();
                    request.SalesOrder[0].ExternalSalesPartnerSO = externalSalesPartner;

                }

                if (c.Range["H1"].Value2 != null)
                {

                    description = c.Range["H1"].Value2.ToString();
                    request.SalesOrder[0].Name = new EXTENDED_Name();
                    request.SalesOrder[0].Name.Value = description;

                }

                if (c.Range["I1"].Value2 != null)
                {

                    externalReference = c.Range["I1"].Value2.ToString();
                    request.SalesOrder[0].BuyerID = new BusinessTransactionDocumentID();
                    request.SalesOrder[0].BuyerID.Value = externalReference;

                }

                if (c.Range["J1"].Value2 != null)
                {

                    postingDate = c.Range["J1"].Value2.ToString();
                    request.SalesOrder[0].PostingDate = DateTime.Parse(postingDate);


                }

                if (c.Range["K1"].Value2 != null)
                {
                    requestedDate = c.Range["K1"].Value2.ToString();
   
                    request.SalesOrder[0].RequestedFulfillmentPeriodPeriodTerms = new SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms();
                    request.SalesOrder[0].RequestedFulfillmentPeriodPeriodTerms.actionCode = ActionCode.Item01;
                    request.SalesOrder[0].RequestedFulfillmentPeriodPeriodTerms.actionCodeSpecified = true;
                    request.SalesOrder[0].RequestedFulfillmentPeriodPeriodTerms.StartDateTime = new LOCALNORMALISED_DateTime();
                    request.SalesOrder[0].RequestedFulfillmentPeriodPeriodTerms.StartDateTime.Value = requestedDate + "T05:30:00Z";
                    request.SalesOrder[0].RequestedFulfillmentPeriodPeriodTerms.StartDateTime.timeZoneCode = "UTC-6";



                }

                if (c.Range["L1"].Value2 != null)
                {

                    currency = c.Range["L1"].Value2.ToString();


                }

                if (c.Range["M1"].Value2 != null)
                {

                    deliveryPriority = c.Range["M1"].Value2.ToString();
                    request.SalesOrder[0].DeliveryTerms = new SalesOrderMaintainRequestDeliveryTerms();
                    request.SalesOrder[0].DeliveryTerms.actionCode = ActionCode.Item01;
                    request.SalesOrder[0].DeliveryTerms.DeliveryPriorityCode = deliveryPriority;


                }

                //Initialize Text

                request.SalesOrder[0].TextCollection = new SalesOrderMaintainRequestTextCollection();
                request.SalesOrder[0].TextCollection.actionCode = ActionCode.Item01;

                var textCounter = 0;

                if (c.Range["N1"].Value2 != null)
                {

                    customerNotes = c.Range["N1"].Value2.ToString();
                    if (customerNotes != "")
                    {

                        textCounter++;
                    }


                }

                if (c.Range["O1"].Value2 != null)
                {

                    internalComments = c.Range["O1"].Value2.ToString();

                    if (internalComments != "")
                    {

                        textCounter++;
                    }

                }


                request.SalesOrder[0].TextCollection.Text = new SalesOrderMaintainRequestTextCollectionText[textCounter];

                if (customerNotes != "" && internalComments != "")
                {

                    request.SalesOrder[0].TextCollection.Text[0] = new SalesOrderMaintainRequestTextCollectionText();
                    request.SalesOrder[0].TextCollection.Text[0].actionCode = ActionCode.Item01;
                    request.SalesOrder[0].TextCollection.Text[0].actionCodeSpecified = true;
                    request.SalesOrder[0].TextCollection.Text[0].TypeCode = new TextCollectionTextTypeCode();
                    request.SalesOrder[0].TextCollection.Text[0].TypeCode.Value = "10024";
                    request.SalesOrder[0].TextCollection.Text[0].ContentText = customerNotes;


                    request.SalesOrder[0].TextCollection.Text[1] = new SalesOrderMaintainRequestTextCollectionText();
                    request.SalesOrder[0].TextCollection.Text[1].actionCode = ActionCode.Item01;
                    request.SalesOrder[0].TextCollection.Text[1].actionCodeSpecified = true;
                    request.SalesOrder[0].TextCollection.Text[1].TypeCode = new TextCollectionTextTypeCode();
                    request.SalesOrder[0].TextCollection.Text[1].TypeCode.Value = "10011";
                    request.SalesOrder[0].TextCollection.Text[1].ContentText = internalComments;

                }


                if (customerNotes != "" && internalComments == "")
                {
                    request.SalesOrder[0].TextCollection.Text[0] = new SalesOrderMaintainRequestTextCollectionText();
                    request.SalesOrder[0].TextCollection.Text[0].actionCode = ActionCode.Item01;
                    request.SalesOrder[0].TextCollection.Text[0].actionCodeSpecified = true;
                    request.SalesOrder[0].TextCollection.Text[0].TypeCode = new TextCollectionTextTypeCode();
                    request.SalesOrder[0].TextCollection.Text[0].TypeCode.Value = "10024";
                    request.SalesOrder[0].TextCollection.Text[0].ContentText = customerNotes;

                }

                if (customerNotes == "" && internalComments != "")
                {

                    request.SalesOrder[0].TextCollection.Text[0] = new SalesOrderMaintainRequestTextCollectionText();
                    request.SalesOrder[0].TextCollection.Text[0].actionCode = ActionCode.Item01;
                    request.SalesOrder[0].TextCollection.Text[0].actionCodeSpecified = true;
                    request.SalesOrder[0].TextCollection.Text[0].TypeCode = new TextCollectionTextTypeCode();
                    request.SalesOrder[0].TextCollection.Text[0].TypeCode.Value = "10011";
                    request.SalesOrder[0].TextCollection.Text[0].ContentText = internalComments;

                }



                //Get items from 

                var notNullItems = listaItems.Where(n => n.Range["A1"].Value2 != null);

                var filterItems = notNullItems.Where(n => n.Range["A1"].Value2.ToString() == orderID);


                var totalItems = filterItems.Count();

                request.SalesOrder[0].Item = new SalesOrderMaintainRequestItem[totalItems];
                requestUpdate.SalesOrder[0].Item = new SalesOrderMaintainRequestItem[totalItems];

                var counterItems = 0;
                foreach (Excel.Range range in filterItems)
                {
                    request.SalesOrder[0].Item[counterItems] = new SalesOrderMaintainRequestItem();
                    request.SalesOrder[0].Item[counterItems].actionCode = ActionCode.Item01;

                    requestUpdate.SalesOrder[0].Item[counterItems] = new SalesOrderMaintainRequestItem();
                    requestUpdate.SalesOrder[0].Item[counterItems].actionCode = ActionCode.Item02;

                    var itemOrderID = "";
                    var lineID = "";
                    var product = "";
                    var quantityUnit = "";
                    var quantity = "";
                    var listPrice = "";
                    var listPriceCurrency = "";
                    var discount = "";
                    var requestedDateItem = "";
                    var deliveryRules = "";
                    var productSpecification = "";
                    var shipToItem = "";
                    var shipFrom = "";
                    var taxCountry = "";
                    var taxJurisdiction = "";
                    var taxCode = "";
                    var measureUnit = "";



                    if (range.Range["A1"].Value2 != null)
                    {

                        itemOrderID = range.Range["A1"].Value2.ToString();

                        /*
                        request.SalesOrder[0].ID = new BusinessTransactionDocumentID();
                        request.SalesOrder[0].ID.Value = orderID;
                        */

                    }

                    if (range.Range["B1"].Value2 != null)
                    {

                        lineID = range.Range["B1"].Value2.ToString();
                        request.SalesOrder[0].Item[counterItems].ID = lineID;
                        requestUpdate.SalesOrder[0].Item[counterItems].ID = lineID;



                    }

                    if (range.Range["C1"].Value2 != null)
                    {

                        product = range.Range["C1"].Value2.ToString();
                        request.SalesOrder[0].Item[counterItems].ItemProduct = new SalesOrderMaintainRequestItemProduct();
                        request.SalesOrder[0].Item[counterItems].ItemProduct.ProductID = new NOCONVERSION_ProductID();
                        request.SalesOrder[0].Item[counterItems].ItemProduct.ProductID.Value = product;



                    }

                    if (range.Range["D1"].Value2 != null)
                    {

                        quantity = range.Range["D1"].Value2.ToString();
                        request.SalesOrder[0].Item[counterItems].ItemScheduleLine = new SalesOrderMaintainRequestItemScheduleLine[1];
                        request.SalesOrder[0].Item[counterItems].ItemScheduleLine[0] = new SalesOrderMaintainRequestItemScheduleLine();
                        request.SalesOrder[0].Item[counterItems].ItemScheduleLine[0].actionCode = ActionCode.Item01;
                        request.SalesOrder[0].Item[counterItems].ItemScheduleLine[0].Quantity = new Quantity();
                        request.SalesOrder[0].Item[counterItems].ItemScheduleLine[0].Quantity.unitCode = quantityUnit;




                    }

                    if (range.Range["E1"].Value2 != null)
                    {

                        quantityUnit = range.Range["E1"].Value2.ToString();
                        request.SalesOrder[0].Item[counterItems].ItemScheduleLine[0].Quantity.Value = Decimal.Parse(quantity);



                    }

                    if (range.Range["F1"].Value2 != null)
                    {

                        listPrice = range.Range["F1"].Value2.ToString();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem = new SalesOrderMaintainRequestPriceAndTaxCalculationItem();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.actionCode = ActionCode.Item02;
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainPrice = new SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainPrice();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainPrice.actionCode = ActionCode.Item01;
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainPrice.Rate = new Rate();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainPrice.Rate.DecimalValue = Decimal.Parse(listPrice);
                       


                    }

                    if (range.Range["G1"].Value2 != null)
                    {

                        listPriceCurrency = range.Range["G1"].Value2.ToString();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainPrice.Rate.DecimalValue = Decimal.Parse(listPrice);
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainPrice.Rate.CurrencyCode = listPriceCurrency;

                    }


                    if (range.Range["H1"].Value2 != null)
                    {

                        measureUnit = range.Range["H1"].Value2.ToString();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainPrice.RateBaseQuantityTypeCode = new QuantityTypeCode();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainPrice.RateBaseQuantityTypeCode.Value = measureUnit;




                    }

                    if (range.Range["I1"].Value2 != null)
                    {

                        discount = range.Range["I1"].Value2.ToString();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainDiscount = new SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainDiscount();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainDiscount.actionCode = ActionCode.Item01;
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainDiscount.Rate = new Rate();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.ItemMainDiscount.Rate.DecimalValue = Decimal.Parse(discount);




                    }

                    if (range.Range["J1"].Value2 != null)
                    {
                        
                        requestedDateItem = range.Range["J1"].Value2.ToString();
                        //request.SalesOrder[0].Item[counterItems].ItemScheduleLine = new SalesOrderMaintainRequestItemScheduleLine[1];
                        //request.SalesOrder[0].Item[counterItems].ItemScheduleLine[0].actionCode = ActionCode.Item01;
                        DateTime requestedDateTime = DateTime.Parse(requestedDateItem+"Z");


                        request.SalesOrder[0].Item[counterItems].ItemScheduleLine[0].DateTimePeriod = new UPPEROPEN_LOCALNORMALISED_DateTimePeriod();

                        request.SalesOrder[0].Item[counterItems].ItemScheduleLine[0].DateTimePeriod.StartDateTime = new LOCALNORMALISED_DateTime1();
                        request.SalesOrder[0].Item[counterItems].ItemScheduleLine[0].DateTimePeriod.StartDateTime.Value = requestedDateTime;

                    }

                    if (range.Range["K1"].Value2 != null)
                    {

                        deliveryRules = range.Range["K1"].Value2.ToString();



                    }

                    if (range.Range["L1"].Value2 != null)
                    {

                        productSpecification = range.Range["L1"].Value2.ToString();
                        request.SalesOrder[0].Item[counterItems].ItemProduct.ProductRequirementSpecificationKey = new RequirementSpecificationKey();
                        request.SalesOrder[0].Item[counterItems].ItemProduct.ProductRequirementSpecificationKey.RequirementSpecificationID = new RequirementSpecificationID();
                        request.SalesOrder[0].Item[counterItems].ItemProduct.ProductRequirementSpecificationKey.RequirementSpecificationID.Value = productSpecification;


                    }

                    if (range.Range["M1"].Value2 != null)
                    {

                        shipToItem = range.Range["M1"].Value2.ToString();
                        request.SalesOrder[0].Item[counterItems].ProductRecipientItemParty = new SalesOrderMaintainRequestPartyParty();
                        request.SalesOrder[0].Item[counterItems].ProductRecipientItemParty.PartyID = new PartyID();
                        request.SalesOrder[0].Item[counterItems].ProductRecipientItemParty.PartyID.Value = shipToItem;


                    }

                    if (range.Range["N1"].Value2 != null)
                    {

                        shipFrom = range.Range["N1"].Value2.ToString();
                        request.SalesOrder[0].Item[counterItems].ShipFromItemLocation = new SalesOrderMaintainRequestItemLocation();
                        request.SalesOrder[0].Item[counterItems].ShipFromItemLocation.LocationID = new LocationID();
                        request.SalesOrder[0].Item[counterItems].ShipFromItemLocation.LocationID.Value = shipFrom;




                    }

                    if (range.Range["O1"].Value2 != null)
                    {

                        taxCountry = range.Range["O1"].Value2.ToString();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.CountryCode = taxCountry;




                    }

                    if (range.Range["P1"].Value2 != null)
                    {

                        taxJurisdiction = range.Range["P1"].Value2.ToString();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.TaxJurisdictionCode = new TaxJurisdictionCode();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.TaxJurisdictionCode.listID = taxCountry;
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.TaxJurisdictionCode.Value = taxJurisdiction;




                    }
                    if (range.Range["Q1"].Value2 != null)
                    {

                        taxCode = range.Range["Q1"].Value2.ToString();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.TaxationCharacteristicsCode = new ProductTaxationCharacteristicsCode();
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.TaxationCharacteristicsCode.Value = taxCode;
                        requestUpdate.SalesOrder[0].Item[counterItems].PriceAndTaxCalculationItem.TaxationCharacteristicsCode.listID = taxCountry;


                    }



                    counterItems++;


                }


                SalesOrderMaintainConfirmationBundleMessage_sync response = CallWSTST(request);


                if (response.SalesOrder != null)
                {
                    if (response.SalesOrder.Count() > 0)
                    {
                        //Updating prices
                        requestUpdate.SalesOrder[0].ID = response.SalesOrder[0].ID;
                        SalesOrderMaintainConfirmationBundleMessage_sync responseUpdate = CallWSTST(requestUpdate);

                    }

                }



            }

            outMessageMain.Value2 += "\nFinished, check output tab for further details";


        }

        public SalesOrderMaintainConfirmationBundleMessage_sync CallWSTST(SalesOrderMaintainRequestBundleMessage_sync request)
        {

            Excel.Worksheet outSheet = ((Excel.Worksheet)this.Application.ActiveWorkbook.Sheets[4]);
            Excel.Range outMessage = outSheet.get_Range("A1");

            Excel.Worksheet confSheet = ((Excel.Worksheet)this.Application.ActiveWorkbook.Sheets[3]);
            Excel.Range URL = confSheet.get_Range("B1");
            Excel.Range serviceUser = confSheet.get_Range("B2");
            Excel.Range servicePassword = confSheet.get_Range("B3");

            String sURL = "";
            String sUser = "";
            String sPassword = "";

            if (URL.Value2 != null)
            {

                sURL = URL.Value2.ToString();
            }

            if (serviceUser.Value2 != null)
            {

                sUser = serviceUser.Value2.ToString();
            }

            if (servicePassword.Value2 != null)
            {

                sPassword = servicePassword.Value2.ToString();
            }





            service srv = new service();


            if (sPassword != "" && sUser != "" && sURL != "")
            {
                srv.Url = srv.Url.Replace("my342987.sapbydesign.com", sURL);

                srv.Credentials = new System.Net.NetworkCredential(sUser, sPassword);


                SalesOrderMaintainConfirmationBundleMessage_sync response = srv.MaintainBundle(request);

                if (response.SalesOrder != null)
                {
                    foreach (var returnMessage in response.SalesOrder)
                    {

                        outMessage.Value2 += "\nSales Order: " + returnMessage.ID.Value + " was created!";

                        button1.Text = "Finished";


                    }
                }
                else
                {
                    outMessage.Value2 += "\nAn error occurred";
                }

                if (request.SalesOrder[0].actionCode != ActionCode.Item02)
                {
                    if (response.Log.Item != null)
                    {

                        outMessage.Value2 += "\nLog Messages returned: " + response.Log.Item.Count();



                        foreach (var trag in response.Log.Item.Where(n => n.SeverityCode == "3"))
                        {

                            outMessage.Value2 += "\nSeverity Error";
                            outMessage.Value2 += "\n" + trag.Note;
                            outMessage.Value2 += "\n" + trag.NoteTemplateText;

                        }

                        foreach (var log in response.Log.Item.Where(n => n.SeverityCode != "3"))
                        {
                            outMessage.Value2 += "\nOther Messages";
                            outMessage.Value2 += "\n" + log.Note;
                            outMessage.Value2 += "\n" + log.NoteTemplateText;

                        }

                    }
                }


                return response;
            }

            else
            {
                button1.Text = "Error.. Try again";
                outMessage.Value2 += "\nAuthentication error, please check URL, User and Password located under the configuration Tab.";
                return new SalesOrderMaintainConfirmationBundleMessage_sync();
            }

        }


    }
}
